# LaravelECommerce2023

# How to use

- Clone the repository with git clone
- cp .env.example .env
- Edit .env file to find the database name
- Create the database. Use the database name from  .env  file
- Run composer install
- Run npm install
- Run php artisan migrate --seed (it has some seeded data)
- Run php artisan key:generate
- Run npm run dev
- Run php artisan serve

